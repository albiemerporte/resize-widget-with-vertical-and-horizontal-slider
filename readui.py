
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication


class MainForm:
    def __init__(self):
        self.mainui = loadUi('readui.ui')
        self.mainui.show()
        
        self.mainui.hsliderWidth.setRange(40, 200)
        self.mainui.vsliderHeight.setRange(40, 200)
        self.mainui.hsliderWidth.valueChanged.connect(self.width)
        self.mainui.vsliderHeight.valueChanged.connect(self.height)
        
        self.normalwidth = 340
        self.normalheight = 265
        
    def value(self):
        self.formwidth = self.mainui.hsliderWidth.value()
        self.formheight = self.mainui.vsliderHeight.value()
        
        return self.formwidth, self.formheight
    
    def width(self):
        self.mainui.resize(self.normalwidth + self.value()[0], \
                           self.normalheight + self.value()[1])
        return self.formwidth      #this is returning the original value of function value
    
    def height(self):
        self.mainui.resize(self.normalwidth + self.value()[0], \
                           self.normalheight + self.value()[1])
        return self.formheight     #this is returnin the original value of function value
        

if __name__ == '__main__':
    app = QApplication([])
    main = MainForm()
    app.exec()
    
    